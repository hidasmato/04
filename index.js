var func1 = function (params) {
    console.log("Параметры функции 1: " + params.toString());
    return 1;
}

var func2 = (params) => {
    console.log("Параметры функции 2: " + params.toString());
    return 2;
}

function func3(params) {
    console.log("Параметры функции 3: " + params.toString());
    return 3;
}

console.log(func1("asd"));
console.log(func2("123"));
console.log(func3("o0o"));